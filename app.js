var numSquares = 6;
var colors = [];
var pickedColor;
var squares = document.querySelectorAll(".square");
// var resetButton = document.querySelector("#reset");
// var modeButtons = document.querySelectorAll(".mode");
var messageDisplay = document.querySelector("#message")
var h1 = document.querySelector("h1");
var RGBDisplay = document.getElementById("RGBDisplay");


var colors = ["rgb(255, 0, 0)", "rgb(255, 0, 255)", "rgb(255, 225, 0)", "rgb(255, 0, 255)", "rgb(0, 255, 255)", "rgb(0, 255, 0)"];

// for (i = 0; i < squares.length; i++) {
//   squares[i].style.backgroundColor = colors[i];
//   // squares[i].addEventListener('click', function () {
//   //   alert('options was clicked')
//   // });
// }

function changeColors(color) {
  for (i = 0; i < squares.length; i++) {
    squares[i].style.backgroundColor = color;
    // messageDisplay.textContent = "You are good at guessing!";
  }
}

// function pickColor() {
//   var random = Math.floor(Math.random() * colors.length);
//   console.log(random);
//   return colors[random];
// }

var pickedColor = pickColor();
console.log(pickedColor);

RGBDisplay.textContent = pickedColor;
for (i = 0; i < squares.length; i++) {
  squares[i].style.backgroundColor = colors[i];
  //enable click event on each square.....     
  squares[i].addEventListener('click', function () {
    //if the user selected the colour....         
    var clickedColor = this.style.backgroundColor;
    console.log(clickedColor);
    // var pickedColor = colors[3];
    //check if the selected colour matches the default colour...
    if (clickedColor === pickedColor) {
      changeColors(clickedColor);
      messageDisplay.textContent = "Correct!";
      console.log(messageDisplay.textContent);
    } else {
      this.style.backgroundColor = "#232323";
      messageDisplay.textContent = "Wrong Choice!";
      console.log(messageDisplay.textContent);
    }
  });
}

function pickColor() {
  var random = Math.floor(Math.random() * colors.length);
  console.log(random);
  return colors[random];
}

function generateRandomColors(numSquares) {
  //make an array
  var arr = []
  //repeat num times
  for (var i = 0; i < numSquares; i++) {
    //get random color and push into arr
    arr.push(randomColor())
  }
  console.log(arr);
  //return that array
  return arr;
}

function randomColor() {
  //pick a "red" from 0 - 255
  var r = Math.floor(Math.random() * 256);
  //pick a "green" from  0 -255
  var g = Math.floor(Math.random() * 256);
  //pick a "blue" from  0 -255
  var b = Math.floor(Math.random() * 256);
  console.log(r);
  console.log(g);
  console.log(b);
  return "rgb(" + r + ", " + g + ", " + b + ")";
}